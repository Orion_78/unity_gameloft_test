﻿using UnityEngine;
using System;

public class Destructible : MonoBehaviour {

    public static int LivingUnit = 0;

    private float delayAfterHit;

    public enum HitType
    {
        GetHitRanged,
        GetHitMelee,
        KnockDown,
        Dead,
        Dodge,
        Ready
    };

    public HitType CurrentStatus;

    [HideInInspector]
    public int CurrentHP;
    
    private Animator _animator;

    [SerializeField]
    private AudioClip[] _HitSounds;

    [SerializeField]
    private AudioClip[] _DeadSound;
    

    [SerializeField]
    public int MaxHP;

    public Action GetHitAction = delegate { };
    public Action GetDeadAction = delegate { };

    
    void Start()
    {
        _animator = GetComponent<Animator>();
        SetAlive();
    }

    void DeadRemoveMe()
    {
        Destroy(gameObject);
    }

    void SetAlive()
    {
        LivingUnit++;
        CurrentHP = MaxHP;
        CurrentStatus = HitType.Ready;
    }
    void SetDead()
    {
        LivingUnit--;
        CurrentHP = 0;
        CurrentStatus = HitType.Dead;
        AudioManager.Instance.PlaySound(_DeadSound);
        _animator.SetTrigger("GetDead");
        GetDeadAction();

        if (GetComponent<Player>() == null)
            Invoke("DeadRemoveMe", 10);
    }

    public void GetHit(int dmg, HitType hitType)
    {
        if (CurrentStatus == HitType.Dodge || CurrentStatus == HitType.Dead)
            return;

        CurrentHP -= dmg;

        GetHitAction();

        var u = GetComponent<UnitPlacement>();
        if (u != null)
            u.pointForSelection = 0;

        if (CurrentHP <= 0)
        {
            SetDead();
        }
        else
        {
            AudioManager.Instance.PlaySound(_HitSounds);
            _animator.SetTrigger(hitType.ToString());

            CurrentStatus = hitType;

            switch (hitType)
            {
                case HitType.GetHitRanged:
                case HitType.GetHitMelee:
                    Invoke("SetReady", delayAfterHit);
                    break;
                case HitType.KnockDown:
                case HitType.Dead:
                case HitType.Ready:
                    
                    break;
                default:
                    break;
            }
            
        }
    }

    void SetReady()
    {
        if (CurrentStatus != HitType.Dead)
            CurrentStatus = HitType.Ready;
    }

    public void KnockDownOver()
    {
        if (CurrentStatus != HitType.Dead)
        {

            CurrentStatus = HitType.Ready;

            _animator.SetTrigger("GetUp");
        }
    }
}
