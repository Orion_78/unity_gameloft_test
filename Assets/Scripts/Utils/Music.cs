﻿using UnityEngine;
using System.Collections;

public class Music : MonoBehaviour {

    [SerializeField]
    private AudioClip _music;

	// Use this for initialization
	void Start () {
        AudioManager.Instance.PlayMusic(_music);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
