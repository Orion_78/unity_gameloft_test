﻿using UnityEngine;
using System.Collections;

public class HeroDeath : MonoBehaviour {

    [SerializeField]
    private GameObject _canvasDead;

	// Use this for initialization
	void Start () {
        GetComponent<Destructible>().GetDeadAction += OnDeath;
	}
	void OnDeath()
    {
        _canvasDead.SetActive(true);
    }
}
