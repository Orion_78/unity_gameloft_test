﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIHUD : MonoBehaviour {

    [SerializeField]
    private PlayerScore _playerScore;
    [SerializeField]
    private WavaManager _waveManager;
    [SerializeField]
    private Text _textScore;
    [SerializeField]
    private Text _textTime;


    [SerializeField]
    private Slider _slider;
    [SerializeField]
    private Image _sliderImage;

    [SerializeField]
    private Destructible _playerLife;

    // Update is called once per frame
    void Update () {
        _textScore.text = "Score " + _playerScore.Score + "\n" +
            "X " + _playerScore.Combo;

        _slider.value = (float)_playerLife.CurrentHP / (float)_playerLife.MaxHP;

        _sliderImage.color = Color.Lerp(Color.red, Color.green, _slider.value);

        string _t = "Wave " + _waveManager.CurrentWave + "\n";

        if (_waveManager.timerNextWave != 0)
        {
            string minSec = string.Format("{0}:{1:00}", (int)_waveManager.timerNextWave / 60, (int)_waveManager.timerNextWave % 60);

            _t += "Next Wave " + minSec;
        }

        _textTime.text = _t;
    }
}
