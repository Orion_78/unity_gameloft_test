﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class UIDead : MonoBehaviour {

    [SerializeField]
    private Text _text;

    [SerializeField]
    private PlayerScore _playerScore;

    [SerializeField]
    private WavaManager _waveManager;

    void Update()
    {
        _text.text = "Wave " + _waveManager.CurrentWave + "\n" +
            "Best Combo " + _playerScore.BestCombo;
    }

    public void ButtonRetry()
    {
        SceneManager.LoadScene("Main");
    }
}
