﻿using UnityEngine;
using System;

public class InputsControl : MonoBehaviour {

    public enum InputResult
    {
        SwipeLeft,
        SwipeRight,
        LeftClic,
        RightClic
    };

    [SerializeField]
    private float _minSwipeInputLength;

    private Vector3 _beginTouch;

    public Action<InputResult> InputResultAction = delegate{};

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            _beginTouch = Input.mousePosition;
        }

        if (Input.GetButtonUp("Fire1"))
        {
            Vector3 movement = (_beginTouch - Input.mousePosition);
            float distance = movement.magnitude;

            if (distance > _minSwipeInputLength)
            {
                if (movement.x > 0)
                    InputResultAction(InputResult.SwipeLeft);
                else
                    InputResultAction(InputResult.SwipeRight);
            }
            else
            {
                if (Input.mousePosition.x > (Screen.width / 2))
                    InputResultAction(InputResult.RightClic);
                else
                    InputResultAction(InputResult.LeftClic);

            }
        }
    }
}
