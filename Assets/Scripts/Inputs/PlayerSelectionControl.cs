﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class PlayerSelectionControl : MonoBehaviour {

//    [SerializeField]
//    private GameObject _targetSprite;

    private InputsControl _input;

    [HideInInspector]
    public GameObject _currentSelection;
    
    public static float rotateSpeed = 5;

    public Action<GameObject> ChangeCurrentSelectionAction = delegate { };

    

   

    void Start()
    {
        _input = GetComponent<InputsControl>();
        _input.InputResultAction += ReceiveActionInput;

        // Instantiate the target sprite
   //     _targetSprite = Instantiate<GameObject>(_targetSprite);
    //    _targetSprite.SetActive(false);

        GetComponent<Destructible>().GetDeadAction += OnDeath;
    }
    
    void Update()
    {

        if (_currentSelection != null)
        {
            var targetRotation = Quaternion.LookRotation(_currentSelection.transform.position - transform.position);

            // Smoothly rotate towards the enemy.
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotateSpeed * Time.deltaTime);
            Vector3 e = transform.rotation.eulerAngles;
            e.x = 0;
            e.y += -6;
            transform.rotation = Quaternion.Euler(e);

            Vector3 pos = transform.position;
            pos.y = 0;
            transform.position = pos;
        }
    }

    void OnDeath()
    {
        Change_currentSelection(null);
    }
   

    /// <summary>
    /// Receive swipe selection
    /// </summary>
    /// <param name="result"></param>
    void ReceiveActionInput(InputsControl.InputResult result)
    {
        if (GetComponent<Destructible>().CurrentStatus == Destructible.HitType.Dead)
            return;

        switch (result)
        {
            case InputsControl.InputResult.SwipeLeft:
            case InputsControl.InputResult.SwipeRight:

                UnitPlacement[] enemies = FindObjectsOfType<UnitPlacement>();
                List<UnitPlacement> unitRightDirection = new List<UnitPlacement>();
                    foreach (var e in enemies)
                    {
                        if (_currentSelection != e.gameObject && e._direction == result && e.GetComponent<Destructible>().CurrentStatus != Destructible.HitType.Dead)
                            unitRightDirection.Add(e);
                    }

                unitRightDirection.Sort(UnitPlacement.SortUnitPLacementFunction);
              
                if (unitRightDirection.Count > 0)
                {
                    Change_currentSelection(unitRightDirection[0].gameObject);
                }
                break;
            default:
                break;
        }
    }

    void Change_currentSelection(GameObject newTarget)
    {
        if (_currentSelection != null)
        {
            _currentSelection.transform.GetChild(6).gameObject.GetComponent<Renderer>().material.color = Color.white;
            _currentSelection.GetComponent<Destructible>().GetDeadAction -= CurrentSelectDead;
        }

        _currentSelection = newTarget;

        

        if (_currentSelection == null)
        {
  //          _targetSprite.SetActive(false);
   //         _targetSprite.transform.SetParent(transform);
        }
        else
        {
            _currentSelection.GetComponent<Destructible>().GetDeadAction += CurrentSelectDead;

            _currentSelection.transform.GetChild(6).gameObject.GetComponent<Renderer>().material.color = Color.red;

//            _targetSprite.SetActive(true);

   //         _targetSprite.transform.parent = _currentSelection.transform;
    //        _targetSprite.transform.localPosition = Vector3.zero;
        }

        ChangeCurrentSelectionAction(_currentSelection);
    }

    void CurrentSelectDead()
    {
        ReceiveActionInput(InputsControl.InputResult.SwipeRight);
    }
}
