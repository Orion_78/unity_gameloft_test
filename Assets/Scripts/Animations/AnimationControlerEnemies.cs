﻿using UnityEngine;
using System.Collections;

public class AnimationControlerEnemies : MonoBehaviour {

    private Animator _animator;

    private Vector3 _previousPos;

	// Use this for initialization
	void Start () {
        _animator = GetComponent<Animator>();
        _previousPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 delta = -transform.InverseTransformPoint(_previousPos);

        _animator.SetFloat("VelocityX", delta.x*50);
        _animator.SetFloat("VelocityY", delta.z*50);

        _previousPos = transform.position;

      //  transform.position = Vector3.MoveTowards(transform.position, transform.position + transform.forward * 10, Time.deltaTime);

	}
}
