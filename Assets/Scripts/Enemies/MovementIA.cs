﻿using UnityEngine;
using System.Collections;

public class MovementIA : MonoBehaviour {

    UnitPlacement _unitPlacement;
    Destructible _destructible;

    NavMeshAgent _navAgent;

    UnitPlacement.Distance CurrentlyMovingTo = UnitPlacement.Distance.Out;

    private int MaxCountInRanged = 10;
    private int MaxCountInMelee = 2;

    private static int CountMovingAwayFromMelee = 0;

	// Use this for initialization
	void Start () {
        _unitPlacement = GetComponent<UnitPlacement>();
        _navAgent = GetComponent<NavMeshAgent>();
        _destructible = GetComponent<Destructible>();
    }
	
	// Update is called once per frame
	void Update () {
	    
        if (_unitPlacement.OutOfSight || _unitPlacement._currentDistance == UnitPlacement.Distance.Out)
        {
            MoveTo(UnitPlacement.Distance.Ranged);
        }
        else if (_unitPlacement._currentDistance == UnitPlacement.Distance.Ranged && UnitPlacement.CountUnitMelee < MaxCountInMelee)
        {
            MoveTo(UnitPlacement.Distance.Melee);
        }
        else if (_unitPlacement._currentDistance == UnitPlacement.Distance.Melee && UnitPlacement.CountUnitMelee > MaxCountInMelee - CountMovingAwayFromMelee)
        {
            MoveTo(UnitPlacement.Distance.Ranged);
            CountMovingAwayFromMelee++;
        } 


        // External : longue range
        // Longue range and few on close : close range
        // close range and too much close : long range

        

        if (_destructible.CurrentStatus != Destructible.HitType.Ready)
        {
            _navAgent.SetDestination(transform.position);
        }
        else
        {
            // Smooth look at
            var targetRotation = Quaternion.LookRotation(Player.Instance.transform.position - transform.position);

            // Smoothly rotate towards the enemy.
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, PlayerSelectionControl.rotateSpeed * Time.deltaTime);
            Vector3 e = transform.rotation.eulerAngles;
            e.x = 0;
            //e.y += temp;
            transform.rotation = Quaternion.Euler(e);

//            transform.LookAt(Player.Instance.transform.position);
        }
	}

    void LateUpdate()
    {
        CountMovingAwayFromMelee = 0;
    }

    void MoveTo(UnitPlacement.Distance distanceToMove)
    {
       /* if (CurrentlyMovemingTo == distanceToMove)
        {
            return;
        }*/
        
        
        CurrentlyMovingTo = distanceToMove;
      //  Debug.Log(distanceToMove);
        _navAgent.SetDestination(UnitPlacement.GetRandomPointInRange(distanceToMove));
        
    }

    void MoveToMelee()
    {

    }
}
