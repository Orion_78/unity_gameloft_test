﻿using UnityEngine;
using System.Collections;

public class AttackIA : MonoBehaviour {

   

    [SerializeField]
    private int dmg;

    private Animator _animator;

    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    public void OrderAttack()
    {
        GetComponent<UnitPlacement>().pointForSelection += 1;
        _animator.SetTrigger("Atk");
    }

    #region Animation Callback

    void ClosePerfectParry()
    {

    }

    void CloseParry()
    {

    }

    void OpenParry()
    {

    }

    void OpenPerfectParry()
    {

    }

    void EndAttack()
    {

    }

    void StrikePoint()
    {
        if (Vector3.Distance(transform.position, Player.Instance.transform.position) < UnitPlacement.distanceMid)
            Player.Instance.GetComponent<Destructible>().GetHit(dmg, Destructible.HitType.GetHitMelee);

        GetComponent<UnitPlacement>().pointForSelection -= 1;
    }
    #endregion
}
