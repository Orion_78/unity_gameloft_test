﻿using UnityEngine;
using System.Collections;

public class AttackManagerIA : MonoBehaviour {

    AttackIA _lastAttackingEnemy;

    // Use this for initialization
    void Start()
    {
        InvokeRepeating("Attaque", 2, 1);
    }

    // Update is called once per frame
    void Attaque()
    {
        if (Player.Instance.GetComponent<Destructible>().CurrentStatus == Destructible.HitType.Dead)
            return;

        var e = GameObject.FindObjectsOfType<AttackIA>();

        if (e.Length <= 0)
        {
            return;
        }

        if (e.Length == 1)
        {
            OrderAttack(e[0]);
        }
        else
        {
            AttackIA willAttack = null;

            int atempt = 0;
            while (willAttack == null && atempt < 50)
            {
                atempt++;

                int select = Random.Range(0, e.Length);
                AttackIA tempSelect = e[select];
                if (tempSelect != _lastAttackingEnemy && tempSelect.GetComponent<UnitPlacement>()._currentDistance == UnitPlacement.Distance.Melee &&
                    tempSelect.GetComponent<Destructible>().CurrentStatus == Destructible.HitType.Ready)
                {
                    willAttack = tempSelect;
                }
            }

            OrderAttack(willAttack);

        }
    }

    void OrderAttack(AttackIA attacker)
    {
        if (attacker == null)
            return;
        _lastAttackingEnemy = attacker;
        attacker.OrderAttack();
    }
}
