﻿using UnityEngine;
using System.Collections;

public class WavaManager : MonoBehaviour {

    public int CurrentWave = 1;

    public float timerNextWave;

    [SerializeField]
    private PlayerScore _playerScore;

    [SerializeField]
    private GameObject _unitLight;

    bool firstSpawn = true;

    void Awake()
    {
        Destructible.LivingUnit = 0;
    }

    void Update()
    {
        Debug.Log(Destructible.LivingUnit);
        if (!firstSpawn && (timerNextWave <= 0 || Destructible.LivingUnit <= 1) ||
            firstSpawn && Destructible.LivingUnit <= 1)
        {
            timerNextWave = CurrentWave * 25;
            NextWave();
        }
        else
        {
            if (!firstSpawn)
                timerNextWave -= Time.deltaTime;
        }

        /*
        if (Destructible.LivingUnit < CurrentWave * 2)
        {
            NextWave();
        }*/
    }

    
    void NextWave()
    {
        firstSpawn = false;
        CurrentWave++;
        for(int i =0; i < CurrentWave * 3; i++)
        {
            Spawn();
        }
    }

    void Spawn()
    {
        GameObject spawnPoint = new GameObject();
        spawnPoint.transform.position = Player.Instance.transform.position;

        spawnPoint.transform.position -= Player.Instance.transform.right * Random.Range(30, 50);

        spawnPoint.transform.RotateAround(Player.Instance.transform.position, Vector3.up, Random.Range(-45, 45));

        GameObject enemie = Instantiate(_unitLight, spawnPoint.transform.position, Quaternion.identity) as GameObject;

        enemie.GetComponent<Destructible>().GetHitAction += _playerScore.ActionHit;
        enemie.GetComponent<Destructible>().GetDeadAction += _playerScore.ActionKill;

        DestroyImmediate(spawnPoint);
    }
}
