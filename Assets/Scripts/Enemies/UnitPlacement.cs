﻿using UnityEngine;
using System.Collections;

public class UnitPlacement : MonoBehaviour
{

    public float _angle;
    public InputsControl.InputResult _direction;
    public Distance _currentDistance;
    public int pointForSelection = 0;

    private static float _angleMax = 45;
    public static float distanceMid = 2.12f; // 2.12
    private static float distanceRange = 3.51f; // 3.51
    private static float distanceOut = 6; // 6

    public static int CountUnitRange = 0;
    public static int CountUnitMelee = 0;


    public enum Distance
    {
        Melee,
        Mid,
        Ranged,
        Out
    }

    public bool OutOfSight
    { get; private set; }



    void Update()
    {
        Vector3 from = Player.Instance.transform.right;

        Vector3 to = transform.position - Player.Instance.transform.position;

        if (Vector3.Cross(from, to).y > 0)
            _direction = InputsControl.InputResult.SwipeRight;
        else
            _direction = InputsControl.InputResult.SwipeLeft;

        _angle = Vector3.Angle(from, to);

        OutOfSight = _angle > _angleMax;


        float dist = Vector3.Distance(Player.Instance.transform.position, transform.position);

        if (dist < distanceMid)
        {
            _currentDistance = Distance.Melee;
            CountUnitMelee++;
        }
        else if (dist < distanceRange)
        {
            CountUnitMelee++;
            _currentDistance = Distance.Mid;
        }
        else if (dist < distanceOut)
        {
            CountUnitRange++;
            _currentDistance = Distance.Ranged;
        }
        else
        {
            _currentDistance = Distance.Out;
        }
    }

    void LateUpdate()
    {
        CountUnitMelee = 0;
        CountUnitRange = 0;
    }

    public static Vector3 GetRandomPointInRange(Distance distanceToMove)
    {
        GameObject RandomPoint = new GameObject();
        RandomPoint.transform.position = Player.Instance.transform.position;

        switch (distanceToMove)
        {
            case Distance.Melee:
            case Distance.Mid:
                RandomPoint.transform.position += Player.Instance.transform.right * Random.Range(0, distanceMid);
                break;
            case Distance.Ranged:

                RandomPoint.transform.position += Player.Instance.transform.right * Random.Range(distanceRange, distanceOut);
                break;
            case Distance.Out:
                break;
            default:
                break;
        }

        RandomPoint.transform.RotateAround(Player.Instance.transform.position, Vector3.up, Random.Range(-_angleMax, _angleMax));

        Vector3 point = RandomPoint.transform.position;
        DestroyImmediate(RandomPoint);

        return point;
    }


    public static int SortUnitPLacementFunction(UnitPlacement a, UnitPlacement b)
    {
        if (a.pointForSelection == b.pointForSelection)
        {
            if (a._angle >= b._angle)
                return 1;
            else
                return -1;
        }
        else
        {
            if (a.pointForSelection <= b.pointForSelection)
                return 1;
            else
                return -1;
        }
    }
    
}
