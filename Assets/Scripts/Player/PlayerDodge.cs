﻿using UnityEngine;
using System.Collections;

public class PlayerDodge : MonoBehaviour {

    float delayBeforeRedodge = 0;
    
    private float delayBetweenDodge = 0f;


	// Use this for initialization
	void Start () {
        GetComponent<InputsControl>().InputResultAction += InputResultAction;
    }
	
	// Update is called once per frame
	void InputResultAction (InputsControl.InputResult result) {
        switch (result)
        {
            case InputsControl.InputResult.SwipeLeft:
                break;
            case InputsControl.InputResult.SwipeRight:
                break;
            case InputsControl.InputResult.LeftClic:
                PerformDodge();
                break;
            case InputsControl.InputResult.RightClic:
                break;
            default:
                break;
        }
    }

    void PerformDodge()
    {
        if (delayBeforeRedodge < Time.time && GetComponent<Destructible>().CurrentStatus == Destructible.HitType.Ready)
        {
            delayBeforeRedodge = Time.time + delayBetweenDodge;
            GetComponent<Animator>().SetTrigger("Dodge");
            GetComponent<Destructible>().CurrentStatus = Destructible.HitType.Dodge;
        }
    }

    void EndAnim()
    {
        GetComponent<Destructible>().CurrentStatus = Destructible.HitType.Ready;        
    }




}
