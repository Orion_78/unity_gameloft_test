﻿using UnityEngine;
using System.Collections;

public class PlayerScore : MonoBehaviour {

    public int Score;
    public int Combo = 1;

    public int BestCombo;

    int bonusHit = 10;
    int bonusKill = 50;
    
	// Use this for initialization
	void Start () {
        
        foreach(var d in FindObjectsOfType<Destructible>())
        {
            if (d.gameObject != Player.Instance.gameObject)
            {
                d.GetDeadAction += ActionKill;
                d.GetHitAction += ActionHit;
            }
            else
            {
                d.GetHitAction += PlayerGetHit;
                d.GetComponent<PlayerAtkCombo>().AttaqueFailAction += PlayerGetHit;
            }
        }
    }

    void PlayerGetHit()
    {
        Combo = 1;
    }

    public void ActionHit()
    {
        Score += Combo * bonusHit;

        if (BestCombo < Combo)
        {
            BestCombo = Combo;
        }
        Combo++;
    }
    public void ActionKill()
    {
        Score += Combo * bonusKill;
    }
	
}
