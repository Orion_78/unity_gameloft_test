﻿using UnityEngine;
using System.Collections;
using System;
/// <summary>
/// Special behaviour :
/// Perfect timing (after strike point, before close buffer) start next atk immediatly !
/// </summary>
public class PlayerAtkCombo : MonoBehaviour {

    enum AttaqueState
    {
        Idle,
        Buffer,
        StrikePoint,
       CloseBuffer
    }

    AttaqueState _currentAtkState = AttaqueState.Idle;
    bool _orderAtk = false;
 //   Destructible.HitType _currentHitType;

    int CurrentComboPosition = 0;




    [SerializeField]
    private int dmg;
    

    
    
    #region Init and NavMesh
    [SerializeField]
    private Animator _animator;
    [SerializeField]
    private InputsControl _inputControl;
    [SerializeField]
    private PlayerSelectionControl _playerSelectionControl;

    public Action AttaqueFailAction = delegate { };
//    [SerializeField]
//    private NavMeshAgent _navMeshAgent;


    
    void Start()
    {
        _inputControl.InputResultAction += ReceiveActionInput;

        _playerSelectionControl.ChangeCurrentSelectionAction += ReceiveChangeSelection;
    }

    float timer;
    AttaqueState _previousAtkState;
    //Avoir external animation broke
    void Update()
    {
        if (_previousAtkState == _currentAtkState)
        {
            timer += Time.deltaTime;
        }
        else
        {
            _previousAtkState = _currentAtkState;
            timer = 0;
        }


        if (timer > 1)
        {
            _currentAtkState = AttaqueState.Idle;
        }
    }

    void ReceiveChangeSelection(GameObject newSelection)
    {
        if (newSelection == null)
            return;

//        _navMeshAgent.SetDestination(newSelection.transform.position);
        

        PlayerInputHit(newSelection);
    }
    #endregion

    void ReceiveActionInput(InputsControl.InputResult result)
    {
        switch (result)
        {
            case InputsControl.InputResult.RightClic:
                GameObject toHit = _playerSelectionControl._currentSelection;

                if (toHit != null)
                {
                    PlayerInputHit(toHit);
                }
                break;
            default:
                break;
        }
    }
    
    public void PlayerInputHit(GameObject toHit)
    {
        switch (_currentAtkState)
        {
            case AttaqueState.Idle:
                PerformAtk();
                break;
            case AttaqueState.Buffer:
                _orderAtk = true;
                break;
            case AttaqueState.StrikePoint:
                if (!_orderAtk)
                    PerformAtk();
                break;
            case AttaqueState.CloseBuffer:
                PerformAtk();
                break;
            default:
                break;
        }
    }
    
    void PerformAtk()
    {
        _orderAtk = false;

        if (GetComponent<Destructible>().CurrentStatus != Destructible.HitType.Ready)
            return;

        GameObject toHit = _playerSelectionControl._currentSelection;

        UnitPlacement up = toHit.GetComponent<UnitPlacement>();


        if (CurrentComboPosition == 0)
        {

            switch (up._currentDistance)
            {
                case UnitPlacement.Distance.Melee:
                case UnitPlacement.Distance.Mid:
                case UnitPlacement.Distance.Ranged:
                    _animator.SetTrigger(up._currentDistance.ToString());
                    break;
                case UnitPlacement.Distance.Out:
                    _animator.SetTrigger(UnitPlacement.Distance.Ranged.ToString());
                    FailAttaque();

                    /* TODO Dash 

                      _animator.SetTrigger(up._currentDistance.ToString());

                      _currentAtkState = AttaqueState.Buffer;*/
                    break;
                default:
                    break;
            }
        }
        else
        {
            _animator.SetTrigger("ContinuAtk");
        }

        
        CurrentComboPosition = (CurrentComboPosition + 1) % 4;
    }

    void FailAttaque()
    {
        Debug.Log("fail");
        CurrentComboPosition = 0;
        AttaqueFailAction();
    }

    
    #region Animation Event
    void OpenBuffer()
    {
        _currentAtkState = AttaqueState.Buffer;
    }

   

    void StrikePoint(float value)
    {
        _currentAtkState = AttaqueState.StrikePoint;

        GameObject toHit = _playerSelectionControl._currentSelection;
        if (toHit != null)
        {
            Destructible d = toHit.GetComponent<Destructible>();
            if (d.CurrentStatus == Destructible.HitType.Dead || d.CurrentStatus == Destructible.HitType.KnockDown)
            {
                FailAttaque();
            }
            else
            {
                if (value == 0.1)
                {
                    d.GetHit(dmg, Destructible.HitType.GetHitRanged);
                }
                else if (value == -1)
                {
                    d.GetHit(dmg, Destructible.HitType.KnockDown);
                }
                else
                {
                    d.GetHit(dmg, Destructible.HitType.GetHitMelee);
                }

            }
        }
    }

    void CloseBuffer()
    {
        _currentAtkState = AttaqueState.CloseBuffer;

        if (_orderAtk)
        {
            PerformAtk();
        }
    }

    void EndAttack()
    {
        
        FailAttaque();
        _currentAtkState = AttaqueState.Idle;
    }

    void KnockDownStrikePoint()
    {
        StrikePoint(-1);
    }

    void LaunchArrow()
    {

    }

    void DashEnd()
    {
        _currentAtkState = AttaqueState.Idle;
    }
    #endregion
}
